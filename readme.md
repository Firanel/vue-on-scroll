# Vue onscroll

Adds `v-on-scroll` directive to vue.  
Directive calls function or toggles class when element changes visibility.


## Options and Modifiers

| Option | Type | Default | Description |
| --- | --- | --- | --- |
| `useCapture` | bool | false | Capture scroll events anywhere in the DOM. |
| `baseClass` | string | | Add this class to all elements with the directive. |
| `directive` | string | on-scroll | Use custom directive name. |
| `framerate` | number | 20 | Max framerate for checking visibility.<br/>Lower values increase performace higher values increase precision.<br/>Suggested range: 10 - 60 |

| Modifier | Description |
| --- | --- |
| `capture` | Capture scroll events anywhere in the DOM. |
| `repeat` | Perform action every time visibility changes, not only the first time. |


## Init

```js
import Vue from 'vue'
import VueOnScroll from 'vue-on-scroll'
Vue.use(VueOnScroll, {useCapture: true})
```


## Examples

Toggle `visible` class every time.
```html
<p v-on-scroll.repeat>Hello world</p>
```

Call `onscroll` method the first time only.
```html
<p v-on-scroll="onscroll">Hello world</p>
```

Toggle `animate` class the first time only, when any element is scrolled.
```html
<img v-on-scroll.capture="'animate'" src="some/image.bmp"/>
```