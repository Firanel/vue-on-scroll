function isInViewport(element) {
    const rect = element.getBoundingClientRect()
    return (
        rect.bottom >= 0 &&
        rect.left >= 0 &&
        rect.top <= (window.innerHeight || document.documentElement.clientHeight) &&
        rect.right <= (window.innerWidth || document.documentElement.clientWidth)
    )
}

export default {
    install(Vue, options = {}) {
        const baseClass = options?.baseClass
        const useCapture = options === true ? true : options?.useCapture
        const timeout = 1000 / (options?.framerate || 20)

        Vue.directive(options?.directive || 'on-scroll', {
            inserted(el, binding) {
                if (baseClass)
                    el.classList.add(baseClass)

                const value = binding.value || 'visible'
                let action
                if (value instanceof Function)
                    action = value
                else
                    action = () => el.classList.toggle(value)

                const repeat = binding.modifiers.repeat
                let wasInViewport = false
                let waiting = false
                const f = () => {
                    if (waiting === true)
                        return
                    waiting = true

                    const inViewport = isInViewport(el)
                    if (inViewport !== wasInViewport) {
                        action()

                        if (inViewport && !repeat)
                            window.removeEventListener('scroll', f, binding.modifiers.capture || useCapture)

                        wasInViewport = inViewport
                    }

                    setTimeout(() => waiting = false, timeout)
                }
                window.addEventListener('scroll', f, binding.modifiers.capture || useCapture)
                setTimeout(f, 100)
            }
        })
    }
}
